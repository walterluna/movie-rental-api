import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Roles } from '../../auth/decorators/roles.decorator';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { RolesAuthGuard } from '../../auth/guards/roles-auth.guard';
import { WhiteListGuard } from '../../auth/guards/whitelist-auth.guard';
import { MovieTagsDoc } from '../docs/movie-tags.doc';
import { MovieDoc } from '../docs/movie.doc';
import { AddMovieTagsDto } from '../dto/add-movie-tags.dto';
import { CreateMovieDTO } from '../dto/create-movie.dto';
import { MovieQueryParamsDto } from '../dto/movie-query-params.dto';
import { updateMovieDto } from '../dto/update-movie.dto';
import { Movie } from '../entity/movies.entity';
import { MovieTagsService } from '../services/movie-tags.service';
import { MoviesService } from '../services/movies.service';

@Controller('movies')
export class MoviesController {
  constructor(
    private readonly moviesService: MoviesService,
    private readonly movieTagsService: MovieTagsService
  ) {}

  @Get()
  listmovies(@Query() query: MovieQueryParamsDto): Promise<MovieDoc[]> {
    return this.moviesService.listmovies(query);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, WhiteListGuard, RolesAuthGuard)
  @Roles('admin')
  @Post()
  createMovie(@Body() createMovieDto: CreateMovieDTO): Promise<MovieDoc> {
    return this.moviesService.createMovie(createMovieDto);
  }

  @Get(':id')
  getMovie(@Param('id', ParseIntPipe) id: number): Promise<MovieDoc> {
    return this.moviesService.getMovie(id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, WhiteListGuard, RolesAuthGuard)
  @Roles('admin')
  @Patch(':id')
  updateMovie(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateMovieDto: updateMovieDto
  ): Promise<Movie> {
    return this.moviesService.updateMovie(id, updateMovieDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, WhiteListGuard, RolesAuthGuard)
  @Roles('admin')
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  deleteMovie(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.moviesService.deleteMovie(id);
  }
  @Get(':movieId/tags')
  getmovietags(
    @Param('movieId', ParseIntPipe) movieId: number
  ): Promise<MovieTagsDoc> {
    return this.movieTagsService.getMovieTags(movieId);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, WhiteListGuard, RolesAuthGuard)
  @Roles('admin')
  @Patch(':movieId/tags')
  addTags(
    @Body() addmovieTagsDto: AddMovieTagsDto,
    @Param('movieId', ParseIntPipe) movieId: number
  ): Promise<MovieTagsDoc> {
    return this.movieTagsService.patchMovieTags(addmovieTagsDto, movieId);
  }
}
