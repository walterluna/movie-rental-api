import { BadRequestException } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class MovieQueryParamsDto {
  @IsString({ each: true })
  @IsOptional()
  @Transform((tags) => {
    if (typeof tags === 'string') return [tags];
    return tags;
  })
  tags?: string[];

  @IsString()
  @IsOptional()
  title?: string;

  @Transform((it) => {
    switch (String(it).toLowerCase()) {
      case 'true':
        return true;
      case 'false':
        return false;
      default:
        throw new BadRequestException(
          "Parameter 'available' must be true or false"
        );
    }
  })
  @IsBoolean()
  @IsOptional()
  available?: boolean = true;

  @IsString()
  @Transform((sort: string) => {
    switch (sort.toLowerCase()) {
      case 'title':
        return '"movies_title"';
      case 'likes':
        return '"movies_likes"';
      case '"movies_title"':
      case '"movies_likes"':
        return sort;
      default:
        throw new BadRequestException(
          "Parameter 'sort' must be title or likes"
        );
    }
  })
  @IsOptional()
  sortBy?: string = 'title';

  @IsString()
  @Transform((order: string) => {
    switch (order.toUpperCase()) {
      case 'ASC':
        return 'ASC';
      case 'DESC':
        return 'DESC';
      default:
        throw new BadRequestException("Parameter 'order' must be ASC or DESC");
    }
  })
  @IsOptional()
  order?: string = 'ASC';
}
