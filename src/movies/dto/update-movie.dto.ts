import { PartialType } from '@nestjs/swagger';
import { CreateMovieDTO } from './create-movie.dto';

export class updateMovieDto extends PartialType(CreateMovieDTO) {}
