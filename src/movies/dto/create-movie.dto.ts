import {
  IsString,
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsInt,
  IsBoolean,
  IsUrl,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { BadRequestException } from '@nestjs/common';

export class CreateMovieDTO {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @IsUrl()
  @IsNotEmpty()
  poster: string;

  @IsInt()
  @IsPositive()
  stock: number;

  @IsUrl()
  @IsNotEmpty()
  trailer: string;

  @IsNumber()
  @IsPositive()
  salePrice: number;

  @IsNumber()
  @IsPositive()
  rentalPrice: number;

  @Transform((it) => {
    switch (String(it).toLowerCase()) {
      case 'true':
        return true;
      case 'false':
        return false;
      default:
        throw new BadRequestException(
          "Parameter 'available' must be true or false"
        );
    }
  })
  @IsBoolean()
  available: boolean;
}
