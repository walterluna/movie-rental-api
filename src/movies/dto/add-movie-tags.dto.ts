import { IsString } from 'class-validator';

export class AddMovieTagsDto {
  @IsString({ each: true })
  tags: string[];
}
