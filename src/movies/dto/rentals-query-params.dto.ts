import { BadRequestException } from '@nestjs/common';
import { Transform } from 'class-transformer';
import { IsOptional, IsString } from 'class-validator';

export class RentalQueryParams {
  @Transform((it) => {
    switch (String(it).toLowerCase()) {
      case 'true':
        return 'true';
      case 'false':
        return 'false';
      default:
        throw new BadRequestException(
          "Parameter 'active' must be true or false"
        );
    }
  })
  @IsString()
  @IsOptional()
  active?: string;
}
