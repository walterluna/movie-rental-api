import { Type } from 'class-transformer';
import { IsNumber, IsPositive, ValidateNested } from 'class-validator';

export class AcquireMoviesDto {
  @ValidateNested({ each: true })
  @Type(() => MovieDetail)
  movieDetails: MovieDetail[];
}

class MovieDetail {
  @IsNumber()
  @IsPositive()
  movieId: number;

  @IsNumber()
  @IsPositive()
  qty: number;
}
