import { Exclude, Expose, Transform } from 'class-transformer';

@Exclude()
export class MovieTagsDoc {
  @Expose({ name: 'id' })
  id: number;

  @Expose({ name: 'title' })
  title: string;

  @Expose({ name: 'description' })
  description: string;

  @Expose({ name: 'tags' })
  @Transform((tags) => tags.map((tag) => tag.name))
  tags: string[];
}
