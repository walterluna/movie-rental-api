import { Exclude, Expose, Transform } from 'class-transformer';
import { Movie } from '../entity/movies.entity';

@Exclude()
export class AcquireDetailsDoc {
  @Expose({ name: 'movie' })
  @Transform((movie: Movie) => movie?.id)
  movie?: any;

  @Expose({ name: 'quantity' })
  quantity: number;

  @Expose({ name: 'subtotal' })
  subtotal: number;
}
