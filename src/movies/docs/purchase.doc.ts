import { Exclude, Expose, plainToClass, Transform } from 'class-transformer';
import { AcquireDetailsDoc } from './acquire-details.doc';
@Exclude()
export class PurchaseDoc {
  @Expose({ name: 'id' })
  id: number;

  @Expose({ name: 'purchaseDate' })
  @Transform((date) => new Date(date).toISOString())
  purchaseDate: Date;

  @Expose({ name: 'total' })
  total: number;

  @Expose({ name: 'purchaseDetails' })
  @Transform((details) =>
    details?.map((det) => {
      return plainToClass(AcquireDetailsDoc, det);
    })
  )
  purchaseDetails?: AcquireDetailsDoc[];
}
