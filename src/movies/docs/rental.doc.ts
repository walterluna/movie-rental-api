import { Exclude, Expose, plainToClass, Transform } from 'class-transformer';
import { AcquireDetailsDoc } from './acquire-details.doc';

@Exclude()
export class RentalDoc {
  @Expose({ name: 'id' })
  id: number;

  @Expose({ name: 'isActive' })
  isActive: boolean;

  @Expose({ name: 'rentalDetails' })
  @Transform((details) =>
    details?.map((det) => {
      return plainToClass(AcquireDetailsDoc, det);
    })
  )
  rentalDetails?: AcquireDetailsDoc[];

  @Expose({ name: 'total' })
  total: number;

  @Expose({ name: 'rentalBeginDate' })
  @Transform((date) => new Date(date).toISOString())
  rentalBeginDate: Date;

  @Expose({ name: 'rentalEndDate' })
  @Transform((date) => new Date(date).toISOString())
  rentalEndDate: Date;

  @Expose({ name: 'returnDate' })
  @Transform((date) => {
    if (date) {
      return new Date(date).toISOString();
    }
    return null;
  })
  returnDate: Date;
}
