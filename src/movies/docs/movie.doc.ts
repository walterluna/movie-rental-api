import { Expose } from 'class-transformer';

export class MovieDoc {
  @Expose({ name: 'id' })
  id: number;

  @Expose({ name: 'title' })
  title: string;

  @Expose({ name: 'description' })
  description: string;

  @Expose({ name: 'poster' })
  poster: string;

  @Expose({ name: 'trailer' })
  trailer: string;

  @Expose({ name: 'likes' })
  likes: number;

  @Expose({ name: 'salePrice' })
  salePrice: number;

  @Expose({ name: 'rentalPrice' })
  rentalPrice: number;

  @Expose({ name: 'available' })
  available: boolean;
}
