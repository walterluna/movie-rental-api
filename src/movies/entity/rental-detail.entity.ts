import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Movie } from './movies.entity';
import { Rental } from './rental.entity';

@Entity()
export class RentalDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quantity: number;

  @Column({ type: 'float' })
  subtotal: number;

  @ManyToOne(() => Movie, (movie) => movie.rentalDetails)
  movie!: Movie;

  @ManyToOne(() => Rental, (rental) => rental.rentalDetails)
  rental!: Rental;
}
