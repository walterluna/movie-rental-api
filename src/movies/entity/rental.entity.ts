import { User } from '../../users/entity/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { RentalDetail } from './rental-detail.entity';

@Entity()
export class Rental {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  total: number;

  @Column({ default: true })
  isActive: boolean;

  @CreateDateColumn({ type: 'date' })
  rentalBeginDate: Date;

  @Column({ type: 'date' })
  rentalEndDate: Date;

  @Column({ type: 'date', nullable: true })
  returnDate: Date;

  @ManyToOne(() => User, (user) => user.rentals)
  user!: User;

  @OneToMany(() => RentalDetail, (rentalDetail) => rentalDetail.rental)
  rentalDetails!: RentalDetail[];

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date;
}
