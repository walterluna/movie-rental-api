import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PurchaseDetail } from './purchase-detail.entity';
import { RentalDetail } from './rental-detail.entity';
import { Tag } from './tags.entity';

@Entity()
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  poster: string;

  @Column()
  stock: number;

  @Column()
  trailer: string;

  @Column('float')
  salePrice: number;

  @Column('float')
  rentalPrice: number;

  @Column({ default: 0 })
  likes: number;

  @Column()
  available: boolean;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date;

  @OneToMany(() => PurchaseDetail, (purchaseDetail) => purchaseDetail.movie, {
    cascade: true,
  })
  purchaseDetails: PurchaseDetail[];

  @OneToMany(() => RentalDetail, (rentalDetail) => rentalDetail.movie)
  rentalDetails: RentalDetail[];

  @ManyToMany(() => Tag, (tag) => tag.movies)
  tags: Tag[];
}
