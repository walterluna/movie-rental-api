import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Movie } from './movies.entity';
import { Purchase } from './purchase.entity';

@Entity()
export class PurchaseDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  quantity: number;

  @Column({ type: 'float' })
  subtotal: number;

  @ManyToOne(() => Movie, (movie) => movie.purchaseDetails)
  movie!: Movie;

  @ManyToOne(() => Purchase, (purchase) => purchase.purchaseDetails)
  purchase!: Purchase;
}
