import { Test, TestingModule } from '@nestjs/testing';
import { MovieTagsService } from './movie-tags.service';

describe('MovieTagsService', () => {
  let service: MovieTagsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MovieTagsService],
    }).compile();

    service = module.get<MovieTagsService>(MovieTagsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
