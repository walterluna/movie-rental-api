import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Movie } from '../entity/movies.entity';
import { AcquireMoviesDto } from '../dto/acquire-movies.dto';
import { Repository } from 'typeorm';
import { RentalDetail } from '../entity/rental-detail.entity';
import { Rental } from '../entity/rental.entity';
import { UsersService } from 'src/users/services/users.service';
import { plainToClass } from 'class-transformer';
import { RentalDoc } from '../docs/rental.doc';
import { RentalQueryParams } from '../dto/rentals-query-params.dto';
import { UtilsService } from 'src/utils/utils.service';

@Injectable()
export class MovieRentalService {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepo: Repository<Movie>,
    @InjectRepository(Rental)
    private readonly rentalRepository: Repository<Rental>,
    @InjectRepository(RentalDetail)
    private readonly rentalDetailsRepo: Repository<RentalDetail>,
    private readonly usersService: UsersService,
    private readonly utilsService: UtilsService
  ) {}

  async makeRental(
    rentMoviesDto: AcquireMoviesDto,
    userId: number
  ): Promise<RentalDoc> {
    const rentalDetails: RentalDetail[] = await Promise.all(
      rentMoviesDto.movieDetails.map(async (mov) => {
        const movie = await this.movieRepo.findOne({ id: mov.movieId });
        if (!movie) {
          throw new BadRequestException(
            `Movie with id #${mov.movieId} not found`
          );
        }
        if (!movie.available) {
          throw new BadRequestException(
            `Movie with id #${mov.movieId} is not available`
          );
        }
        if (movie.stock < mov.qty) {
          throw new BadRequestException(
            ` Movie with id ${mov.movieId} doesn't have enoguh stock`
          );
        }
        movie.stock -= mov.qty;
        if (movie.stock === 0) movie.available = false;
        await this.movieRepo.save(movie);
        return this.rentalDetailsRepo.save({
          movie,
          quantity: mov.qty,
          subtotal: mov.qty * movie.rentalPrice,
        });
      })
    );
    const rentalEndDate = new Date();
    rentalEndDate.setDate(rentalEndDate.getDate() + 7);
    const rental = await this.rentalRepository.save({
      rentalEndDate,
      rentalDetails,
      user: await this.usersService.findUser({ id: userId }),
      total: rentalDetails.reduce((acc, detail) => acc + detail.subtotal, 0),
    });
    const result = plainToClass(RentalDoc, rental);
    this.utilsService.sendMail({
      from: '"Movie Rental" <MovieRental@trainne.com>', // sender address
      to: rental.user.email, // list of receivers
      subject: 'Your new rental has been succesful ✔', // Subject line
      text: "Here's some useful info:", // plain text body
      html: JSON.stringify(result), // html body
    });
    return result;
  }

  async getRentals(
    userId: number,
    qParams: RentalQueryParams
  ): Promise<RentalDoc[]> {
    const queryBuilder = this.rentalRepository
      .createQueryBuilder()
      .select('rentals')
      .from(Rental, 'rentals')
      .leftJoinAndSelect('rentals.user', 'user')
      .where('rentals.user=:userId', { userId });

    if (qParams.active) {
      queryBuilder.andWhere('rentals.isActive =:active', {
        active: qParams.active,
      });
    }
    const rentals = await queryBuilder.getMany();
    if (!rentals) return [];
    return rentals.map((rental) => plainToClass(RentalDoc, rental));
  }

  async getOneRental(rentalId: number, userId: number): Promise<RentalDoc> {
    const rental = await this.rentalRepository.findOne(
      { id: rentalId },
      { relations: ['user'] }
    );
    if (!rental) {
      throw new NotFoundException(`Rental with id #${rentalId} doesn't exist`);
    }
    if (rental.user.id !== userId) {
      throw new UnauthorizedException();
    }
    const rentalDetails = await this.rentalDetailsRepo.find({
      relations: ['movie'],
      where: { rental },
    });
    rental.rentalDetails = rentalDetails;
    return plainToClass(RentalDoc, rental);
  }

  async returnOneRental(rentalId: number, userId: number) {
    const rental = await this.rentalRepository.findOne(
      { id: rentalId },
      { relations: ['user'] }
    );
    if (!rental) {
      throw new NotFoundException(`Rental with id #${rentalId} doesn't exist`);
    }
    if (rental.user.id !== userId) {
      throw new UnauthorizedException();
    }
    rental.isActive = false;
    const savedRental = await this.rentalRepository.save(rental);

    return {
      data: {
        message: 'Your rental has been succesfully returned',
        rental: plainToClass(RentalDoc, savedRental),
      },
    };
  }
}
