import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { UsersService } from '../../users/services/users.service';
import { Repository } from 'typeorm';
import { PurchaseDoc } from '../docs/purchase.doc';
import { Purchase } from '../entity/purchase.entity';
import { Movie } from '../entity/movies.entity';
import { AcquireMoviesDto } from '../dto/acquire-movies.dto';
import { PurchaseDetail } from '../entity/purchase-detail.entity';
import { UtilsService } from 'src/utils/utils.service';

@Injectable()
export class MoviePurchaseService {
  constructor(
    @InjectRepository(Purchase)
    private readonly purchaseRepository: Repository<Purchase>,
    @InjectRepository(PurchaseDetail)
    private readonly purchaseDetailsRepo: Repository<PurchaseDetail>,
    @InjectRepository(Movie)
    private readonly movieRepo: Repository<Movie>,
    private readonly usersService: UsersService,
    private readonly utilsService: UtilsService
  ) {}

  async makePurchase(
    purchaseMoviesDto: AcquireMoviesDto,
    userId: number
  ): Promise<PurchaseDoc> {
    const purchaseDetails: PurchaseDetail[] = await Promise.all(
      purchaseMoviesDto.movieDetails.map(async (mov) => {
        const movie = await this.movieRepo.findOne({ id: mov.movieId });
        if (!movie) {
          throw new BadRequestException(
            `Movie with id #${mov.movieId} not found`
          );
        }
        if (!movie.available) {
          throw new BadRequestException(
            `Movie with id #${mov.movieId} is not available`
          );
        }
        if (movie.stock < mov.qty) {
          throw new BadRequestException(
            ` Movie with id ${mov.movieId} doesn't have enoguh stock`
          );
        }
        movie.stock -= mov.qty;
        if (movie.stock === 0) movie.available = false;
        await this.movieRepo.save(movie);
        return this.purchaseDetailsRepo.save({
          movie,
          quantity: mov.qty,
          subtotal: mov.qty * movie.salePrice,
        });
      })
    );
    const purchase = await this.purchaseRepository.save({
      purchaseDetails,
      user: await this.usersService.findUser({ id: userId }),
      total: purchaseDetails.reduce((acc, detail) => acc + detail.subtotal, 0),
    });
    const result = plainToClass(PurchaseDoc, purchase);
    this.utilsService.sendMail({
      from: '"Movie Rental" <MovieRental@trainne.com>',
      to: purchase.user.email,
      subject: 'Your new purchase has been succesful ✔',
      text: "Here's some useful info:",
      html: JSON.stringify(result),
    });
    return result;
  }

  async getPurchases(userId: number): Promise<PurchaseDoc[]> {
    const user = await this.usersService.findUser(
      { id: userId },
      { relations: ['purchases'] }
    );
    if (!user.purchases) return [];
    return user.purchases.map((purchase) =>
      plainToClass(PurchaseDoc, purchase)
    );
  }

  async getOnePurchase(
    purchaseId: number,
    userId: number
  ): Promise<PurchaseDoc> {
    const purchase = await this.purchaseRepository.findOne(
      { id: purchaseId },
      { relations: ['user'] }
    );
    if (!purchase) {
      throw new NotFoundException(
        `Purchase with id #${purchaseId} doesn't exist`
      );
    }
    if (purchase.user.id !== userId) {
      throw new UnauthorizedException();
    }
    const purchaseDetails = await this.purchaseDetailsRepo.find({
      relations: ['movie'],
      where: { purchase },
    });
    purchase.purchaseDetails = purchaseDetails;
    return plainToClass(PurchaseDoc, purchase);
  }
}
