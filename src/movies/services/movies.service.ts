import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { MovieDoc } from '../docs/movie.doc';
import { CreateMovieDTO } from '../dto/create-movie.dto';
import { MovieQueryParamsDto } from '../dto/movie-query-params.dto';
import { updateMovieDto } from '../dto/update-movie.dto';
import { Movie } from '../entity/movies.entity';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private readonly moviesRepository: Repository<Movie>
  ) {}

  async createMovie(createMovieDto: CreateMovieDTO): Promise<MovieDoc> {
    const newMovie = await this.moviesRepository.save(createMovieDto);
    return plainToClass(MovieDoc, newMovie, { excludeExtraneousValues: true });
  }

  async listmovies(q: MovieQueryParamsDto): Promise<MovieDoc[]> {
    const queryBuilder = this.moviesRepository
      .createQueryBuilder()
      .select('movies')
      .from(Movie, 'movies')
      .where('movies.available = :av', { av: q.available })
      .orderBy(q.sortBy, q.order as any);

    if (q.title) {
      queryBuilder.andWhere('movies.title LIKE :title', {
        title: `%${q.title}%`,
      });
    }
    if (q.tags) {
      queryBuilder.leftJoinAndSelect('movies.tags', 'tags');
      q.tags.map((tag) => {
        queryBuilder.andWhere('tags.name LIKE :tag', { tag });
      });
    }
    const movies = await queryBuilder.getMany();
    return movies.map((movie) =>
      plainToClass(MovieDoc, movie, { excludeExtraneousValues: true })
    );
  }

  async getMovie(id: number): Promise<MovieDoc> {
    const movie = await this.moviesRepository.findOne({});
    if (!movie) {
      throw new NotFoundException(`Movie with id #${id} not found`);
    }
    return plainToClass(MovieDoc, movie, { excludeExtraneousValues: true });
  }

  async updateMovie(
    id: number,
    updateMovieDto: updateMovieDto
  ): Promise<Movie> {
    const movieToUpdate = await this.moviesRepository.findOne({ id });
    if (!movieToUpdate) {
      throw new NotFoundException(`Movie with id #${id} not found`);
    }
    const updatedMovie = { ...movieToUpdate, ...updateMovieDto };
    return this.moviesRepository.save(updatedMovie);
  }

  async deleteMovie(id: number): Promise<void> {
    const movieToDelete = await this.moviesRepository.findOne({ id });
    if (!movieToDelete) {
      throw new NotFoundException(`Movie with id #${id} not found`);
    }
    const result = await this.moviesRepository.softDelete({ id });
    if (result.affected !== 1) {
      throw new InternalServerErrorException(
        'Something went wrong, try again later'
      );
    }
    return;
  }
}
