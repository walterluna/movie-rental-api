import {
  BadRequestException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/services/users.service';
import { UtilsService } from 'src/utils/utils.service';
import { Repository } from 'typeorm';
import { PurchaseDoc } from '../docs/purchase.doc';
import { Movie } from '../entity/movies.entity';
import { PurchaseDetail } from '../entity/purchase-detail.entity';
import { Purchase } from '../entity/purchase.entity';
import { MoviePurchaseService } from './movie-purchase.service';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
type MockUserService = Partial<Record<keyof UsersService, jest.Mock>>;
type MockUtilsService = Partial<Record<keyof UtilsService, jest.Mock>>;

const createMockRepository = <T = any>(): MockRepository<T> => ({
  findOne: jest.fn(),
  save: jest.fn(),
  find: jest.fn(),
  delete: jest.fn(),
});

const createMockUsersService = (): MockUserService => ({
  findUser: jest.fn(),
});

const createMockUtilsService = (): MockUtilsService => ({
  sendMail: jest.fn(),
});

describe('MoviePurchaseService', () => {
  let service: MoviePurchaseService;
  let usersService: MockUserService;
  let movieRepo: MockRepository;
  let purchaseRepository: MockRepository;
  let purchaseDetailsRepo: MockRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviePurchaseService,
        { provide: UsersService, useValue: createMockUsersService() },
        { provide: UtilsService, useValue: createMockUtilsService() },
        {
          provide: getRepositoryToken(Purchase),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(PurchaseDetail),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Movie),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = module.get<MoviePurchaseService>(MoviePurchaseService);
    usersService = module.get(UsersService);
    movieRepo = module.get<MockRepository>(getRepositoryToken(Movie));
    purchaseRepository = module.get<MockRepository>(
      getRepositoryToken(Purchase)
    );
    purchaseDetailsRepo = module.get<MockRepository>(
      getRepositoryToken(PurchaseDetail)
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('makePurchase', () => {
    const purchaseMoviesDto = { movieDetails: [{ movieId: 1, qty: 3 }] };
    const userId = 1;
    describe("when the movie doesn't exist", () => {
      it('should throw a BadRequestException', async () => {
        movieRepo.findOne.mockReturnValue(undefined);
        try {
          await service.makePurchase(purchaseMoviesDto, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
        }
      });
    });
    describe('when a movie is not available', () => {
      it('should throw a BadRequestException', async () => {
        movieRepo.findOne.mockReturnValue({ available: false });
        try {
          await service.makePurchase(purchaseMoviesDto, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
        }
      });
    });
    describe('when a movie does not have enough stock', () => {
      it('should throw a BadRequestException', async () => {
        movieRepo.findOne.mockReturnValue({ available: true, stock: 1 });
        try {
          await service.makePurchase(purchaseMoviesDto, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
        }
      });
    });
    describe('otherwise', () => {
      it('should return a PurchaseDoc instance', async () => {
        movieRepo.findOne.mockReturnValue({
          available: true,
          stock: 5,
          salePrice: 2.3,
        });
        purchaseDetailsRepo.save.mockReturnValue({ subtotal: 2 });
        usersService.findUser.mockReturnValue({ email: 'user@mail.com' });
        purchaseRepository.save.mockReturnValue({
          user: { email: 'e@mail.com' },
          purchaseDate: new Date(),
        });
        const result = await service.makePurchase(purchaseMoviesDto, userId);
        expect(result).toBeInstanceOf(PurchaseDoc);
      });
    });
  });

  describe('getPurchases', () => {
    const userId = 1;
    describe('when there are no purchases', () => {
      it('should return an empty array', async () => {
        usersService.findUser.mockReturnValue({ purchases: undefined });
        const result = await service.getPurchases(userId);
        expect(result).toEqual([]);
      });
    });
    describe('when there is more than one purchase', () => {
      it('should return an array of PurchaseDoc', async () => {
        usersService.findUser.mockReturnValue({ purchases: [] });
        const result = await service.getPurchases(userId);
        const expected = [];
        expect(result).toEqual(expect.arrayContaining(expected));
      });
    });
  });

  describe('getOnePurchase', () => {
    const purchaseId = 1;
    const userId = 2;
    describe('when the purchase does not exist', () => {
      it('should throw a NotFoundException', async () => {
        purchaseRepository.findOne.mockReturnValue(undefined);
        try {
          await service.getOnePurchase(purchaseId, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(NotFoundException);
        }
      });
    });
    describe('when the purchase belongs to another user', () => {
      it('should throw an UnauthorizedException', async () => {
        purchaseRepository.findOne.mockReturnValue({ user: { id: 5 } });
        try {
          await service.getOnePurchase(purchaseId, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(UnauthorizedException);
        }
      });
    });
    describe('otherwise', () => {
      it('should return a PruchaseDoc', async () => {
        purchaseRepository.findOne.mockReturnValue({
          user: { id: userId },
          purchaseDate: new Date(),
        });
        const result = await service.getOnePurchase(purchaseId, userId);
        expect(result).toBeInstanceOf(PurchaseDoc);
      });
    });
  });
});
