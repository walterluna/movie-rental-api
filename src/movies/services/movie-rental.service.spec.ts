import {
  BadRequestException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from 'src/users/services/users.service';
import { UtilsService } from 'src/utils/utils.service';
import { Repository } from 'typeorm';
import { RentalDoc } from '../docs/rental.doc';
import { Movie } from '../entity/movies.entity';
import { RentalDetail } from '../entity/rental-detail.entity';
import { Rental } from '../entity/rental.entity';
import { MovieRentalService } from './movie-rental.service';

type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;
type MockUserService = Partial<Record<keyof UsersService, jest.Mock>>;
type MockUtilsService = Partial<Record<keyof UtilsService, jest.Mock>>;

const createMockRepository = <T = any>(): MockRepository<T> => ({
  findOne: jest.fn(),
  save: jest.fn(),
  find: jest.fn(),
  delete: jest.fn(),
});

const createMockUsersService = (): MockUserService => ({
  findUser: jest.fn(),
});

const createMockUtilsService = (): MockUtilsService => ({
  sendMail: jest.fn(),
});

describe('MovieRentalService', () => {
  let service: MovieRentalService;
  let usersService: MockUserService;
  let movieRepo: MockRepository;
  let rentalRepository: MockRepository;
  let rentalDetailsRepo: MockRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MovieRentalService,
        { provide: UsersService, useValue: createMockUsersService() },
        { provide: UtilsService, useValue: createMockUtilsService() },
        {
          provide: getRepositoryToken(Movie),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(Rental),
          useValue: createMockRepository(),
        },
        {
          provide: getRepositoryToken(RentalDetail),
          useValue: createMockRepository(),
        },
      ],
    }).compile();

    service = module.get<MovieRentalService>(MovieRentalService);
    usersService = module.get(UsersService);
    movieRepo = module.get<MockRepository>(getRepositoryToken(Movie));
    rentalRepository = module.get<MockRepository>(getRepositoryToken(Rental));
    rentalDetailsRepo = module.get<MockRepository>(
      getRepositoryToken(RentalDetail)
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('makeRental', () => {
    const rentMoviesDto = { movieDetails: [{ movieId: 1, qty: 3 }] };
    const userId = 1;
    describe("when the movie doesn't exist", () => {
      it('should throw a BadRequestException', async () => {
        movieRepo.findOne.mockReturnValue(undefined);
        try {
          await service.makeRental(rentMoviesDto, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
        }
      });
    });
    describe('when a movie is not available', () => {
      it('should throw a BadRequestException', async () => {
        movieRepo.findOne.mockReturnValue({ available: false });
        try {
          await service.makeRental(rentMoviesDto, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
        }
      });
    });
    describe('when a movie does not have enough stock', () => {
      it('should throw a BadRequestException', async () => {
        movieRepo.findOne.mockReturnValue({ available: true, stock: 1 });
        try {
          await service.makeRental(rentMoviesDto, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(BadRequestException);
        }
      });
    });
  });

  describe('getOnePurchase', () => {
    const rentalId = 1;
    const userId = 2;
    describe('when the rental does not exist', () => {
      it('should throw a NotFoundException', async () => {
        rentalRepository.findOne.mockReturnValue(undefined);
        try {
          await service.getOneRental(rentalId, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(NotFoundException);
        }
      });
    });
    describe('when the rental belongs to another user', () => {
      it('should throw an UnauthorizedException', async () => {
        rentalRepository.findOne.mockReturnValue({ user: { id: 5 } });
        try {
          await service.getOneRental(rentalId, userId);
        } catch (error) {
          expect(error).toBeInstanceOf(UnauthorizedException);
        }
      });
    });
  });
});
