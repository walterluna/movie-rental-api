import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { MovieTagsDoc } from '../docs/movie-tags.doc';
import { AddMovieTagsDto } from '../dto/add-movie-tags.dto';
import { Movie } from '../entity/movies.entity';
import { Tag } from '../entity/tags.entity';

@Injectable()
export class MovieTagsService {
  constructor(
    @InjectRepository(Movie)
    private readonly moviesRepository: Repository<Movie>,
    @InjectRepository(Tag)
    private readonly tagsRepository: Repository<Tag>
  ) {}

  private async preLoadTagByName(name: string): Promise<Tag> {
    const tag = await this.tagsRepository.findOne({ name });
    if (tag) return tag;
    return this.tagsRepository.save({ name });
  }

  async patchMovieTags(
    addMovieTagsDto: AddMovieTagsDto,
    movieId: number
  ): Promise<MovieTagsDoc> {
    let movie = await this.moviesRepository.findOne(
      { id: movieId },
      { relations: ['tags'] }
    );
    if (!movie) {
      throw new NotFoundException(`Movie with ID #${movieId} doesn't exist`);
    }
    const tags = await Promise.all(
      addMovieTagsDto.tags.map((tagName) => this.preLoadTagByName(tagName))
    );
    movie.tags = tags;
    await this.moviesRepository.save(movie);
    movie = await this.moviesRepository.findOne(
      { id: movieId },
      { relations: ['tags'] }
    );
    return plainToClass(MovieTagsDoc, movie);
  }

  async getMovieTags(movieId: number): Promise<MovieTagsDoc> {
    const movie = await this.moviesRepository.findOne(
      { id: movieId },
      { relations: ['tags'] }
    );
    if (!movie) {
      throw new NotFoundException(`Movie with id #${movieId} not found`);
    }
    return plainToClass(MovieTagsDoc, movie);
  }
}
