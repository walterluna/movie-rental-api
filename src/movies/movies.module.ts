import { forwardRef, Module } from '@nestjs/common';
import { MoviesService } from './services/movies.service';
import { MoviesController } from './controllers/movies.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Movie } from './entity/movies.entity';
import { WhiteListedTokens } from '../auth/entity/whitelisted-tokens.entity.';
import { UsersModule } from '../users/users.module';
import { Rental } from './entity/rental.entity';
import { MovieRentalService } from './services/movie-rental.service';
import { MoviePurchaseService } from './services/movie-purchase.service';
import { Purchase } from './entity/purchase.entity';
import { Tag } from './entity/tags.entity';
import { MovieTagsService } from './services/movie-tags.service';
import { PurchaseDetail } from './entity/purchase-detail.entity';
import { RentalDetail } from './entity/rental-detail.entity';
import { UtilsModule } from 'src/utils/utils.module';

@Module({
  providers: [
    MoviesService,
    MovieRentalService,
    MoviePurchaseService,
    MovieTagsService,
  ],
  controllers: [MoviesController],
  imports: [
    UtilsModule,
    TypeOrmModule.forFeature([
      Movie,
      Tag,
      Rental,
      RentalDetail,
      Purchase,
      PurchaseDetail,
      WhiteListedTokens,
    ]),
    forwardRef(() => UsersModule),
  ],
  exports: [MovieRentalService, MoviePurchaseService],
})
export class MoviesModule {}
