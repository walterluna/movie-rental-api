import { Purchase } from '../../movies/entity/purchase.entity';
import { Rental } from '../../movies/entity/rental.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Role } from './role.entity';
import { WhiteListedTokens } from 'src/auth/entity/whitelisted-tokens.entity.';
import { UUID } from './uuid.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @ManyToOne(() => Role, (role) => role.users)
  role: Role;

  @OneToMany(() => Rental, (movieRental) => movieRental.user, {
    cascade: true,
  })
  rentals: Rental[];

  @OneToMany(() => Purchase, (moviePurchase) => moviePurchase.user, {
    cascade: true,
  })
  purchases: Purchase[];

  @OneToMany(() => WhiteListedTokens, (token) => token.user, {
    cascade: true,
  })
  whiteLisedTokens: WhiteListedTokens[];

  @OneToMany(() => UUID, (uuid) => uuid.user)
  uuids: UUID[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn({ nullable: true })
  deletedAt?: Date;
}
